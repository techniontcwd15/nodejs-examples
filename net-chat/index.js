const net = require('net');
const sockets = [];

const server = net.createServer();

server.on('connection', (currentSocket) => {
  sockets.push(currentSocket);

  currentSocket.on('data', (data) => {
    sockets.filter((socket) => socket !== currentSocket).forEach(socket => socket.write(data.toString()));
  });
});

server.listen(39981, () => {
  console.log('opened server on', server.address());
})
