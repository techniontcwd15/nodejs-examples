const net = require('net');
const fs = require('fs');
const file = fs.createWriteStream('./file.txt');

const sockets = [];
const server = net.createServer((socket) => {
  socket.on('data', data => {
    file.write(data);
  });
});

server.listen(39981, () => {
  console.log('opened server on', server.address());
})
