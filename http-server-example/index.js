const http = require('http');
const fs = require('fs');

const server = http.createServer();

server.on('request', (req, res) => {
  const file = fs.createReadStream('./file.html');

  console.log(req.headers);
  console.log(req.method);
  console.log(req.url);

  res.writeHead(200);
  file.pipe(res);
});

server.listen(8080)
